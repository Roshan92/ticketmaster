class AddPaymentTypeToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :payment_type, :integer, default: 0
  end
end
